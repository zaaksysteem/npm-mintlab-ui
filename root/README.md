# The `/root` directory

> Some files need to be in the container root for technical 
  reasons, but we don't want them to be under version control
  there for different technical reasons.
  For a build, these files are simply copied to the 
  container root.

## Development

Add this directory as a volume in your `docker-compose.override` file.

## Package manifest files

In the development stage, the `package(-lock)?.json` files 
in the root are replaced with symbolic links because they
might be mutated both on the host (in this directory) or in 
the container (by installing/uninstalling dependencies in 
the root).

## Dot files

Dot files in this directory are rarely changed.
In case you need to, you can simply copy them manually 
to the root directory in the container after editing 
them on the host.
