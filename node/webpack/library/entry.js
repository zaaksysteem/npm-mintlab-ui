/**
 * Generate the main file of the published package.
 * All public modules are named exports.
 */
const { writeFileSync, readFileSync } = require('fs');
const glob = require('glob');
const { parse, resolve } = require('path');

const {
  env: {
    npm_package_config_WEBPACK_PUBLIC_PATH: publicPath,
  },
} = process;

const APP = './App';
const GLOB = './*/*/!(*.*).js';
const DEFAULT_EXPORTS_GLOB = './**/default-exports.js';

// The webpack `publicPath` configuration depends on the consuming application.
// It can be configured in package.json#config, but not after the build.
// Cf. https://webpack.js.org/guides/public-path/#on-the-fly
const BOILERPLATE = `
// GENERATED ENTRY POINT FILE FOR THE WEBPACK PUBLICATION BUILD, DO NOT EDIT.
__webpack_public_path__ = '${publicPath}';
`.trim();

const JOIN = '\n';
const DIR_SLICE_INDEX = -1;
const NOT_FOUND_INDEX = -1;

const isExportsFile = path => 
  path.indexOf('index.js') > NOT_FOUND_INDEX || path.indexOf('default-exports.js') > NOT_FOUND_INDEX;

function generateSingleExport(filePath) {
  const { dir, name } = parse(filePath);
  const [last] = dir
    .split('/')
    .slice(DIR_SLICE_INDEX);

  if (name !== last) {
    return `export {default as ${name}} from '${dir}/${name}';`;
  }

  return `export {default as ${name}} from '${dir}';`;
}

function generateMultiExport(filePath) {
  const { dir } = parse(filePath);
  const absolutePath = resolve(APP, filePath);
  const contents = readFileSync(absolutePath, 'UTF8');

  return contents.replace(/\.\//g, `${dir}/`);
}

function getModulesFromPath(filePath) {
  return isExportsFile(filePath)
    ? generateMultiExport(filePath)
    : generateSingleExport(filePath);
}

const files = [
  ...glob.sync(DEFAULT_EXPORTS_GLOB, { cwd: APP }),
  ...glob.sync(GLOB, { cwd: APP }),
].map(fileName => getModulesFromPath(fileName));

const output = [
  BOILERPLATE,
  files.join(JOIN),
].join(JOIN);

writeFileSync(`${APP}/index.js`, output);
