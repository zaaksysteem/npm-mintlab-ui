const { addDecorator, configure } = require('@storybook/react');
const { withKnobs } = require('@storybook/addon-knobs');
const { checkA11y } = require('@storybook/addon-a11y');

addDecorator(withKnobs);
addDecorator(checkA11y);

const requireAll = requireContext => requireContext.keys().map(requireContext);

function loadStories() {
  const context = require.context('../../App', true, /\.story\.jsx?$/);

  requireAll(context);
}

configure(loadStories, module);
