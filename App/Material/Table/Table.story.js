import { React, stories, boolean } from '../../story';
import {
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableRow,
} from '.';
import Pagination from './../Pagination/Pagination';
import { toggleItem } from '@mintlab/kitchen-sink';

/* eslint-disable no-magic-numbers */

const users = ['Arne', 'Eric', 'Danny'];
const components = ['user', 'case', 'object'];
const ROWS_PER_PAGE_OPTIONS = [5, 10, 25, 50, 100];

const COUNT = 101;
const DATECORRECTION = 1;

const spoofApi = (page, rowsPerPage) => {
  const resultsOnPage = Math.min(rowsPerPage, COUNT - rowsPerPage * page);
  const rows = [];

  for (let index = 0; index < resultsOnPage; index++) {
    const date = `2018-${page + DATECORRECTION}-${index + DATECORRECTION}`;
    const component = components[(index + page) % components.length];

    const caseNumber = page * rowsPerPage + index + 1;

    rows.push({
      caseNumber,
      date,
      description: 'Something happened',
      component,
      user: users[(index + page) % users.length],
      id: caseNumber,
    });
  }

  return rows;
};

const stores = {
  Default: {
    count: COUNT,
    page: 0,
    rows: spoofApi(0, 10),
    rowsPerPage: 10,
    selected: [],
  },
};

const columns = [
  {
    label: 'Zaak #',
    name: 'caseNumber',
  },
  {
    label: 'Datum',
    name: 'date',
  },
  {
    label: 'Omschrijving',
    name: 'description',
  },
  {
    label: 'Component',
    name: 'component',
  },
  {
    label: 'Behandelaar',
    name: 'user',
  },
];

function fetchNewData({ store, page, rowsPerPage }) {
  const delay = 2000;

  store.set({
    page,
    rowsPerPage,
    rows: [],
  });

  function callback() {
    store.set({
      count: COUNT,
      page,
      rowsPerPage,
      rows: spoofApi(page, rowsPerPage),
    });
  }

  window.setTimeout(callback, delay);
}

function updateSelected({ store, id, multiSelect = false, selected }) {
  let newSelected;
  if (multiSelect) {
    const current = selected;
    newSelected = toggleItem(current, id);
  } else {
    newSelected = [id];
  }
  store.set({ selected: newSelected });
}

const changeRowsPerPage = ({ store, rowsPerPage }) => {
  const newRowsPerPage = rowsPerPage.props.value;
  const expectedPage = Math.floor(
    (store.get('page') * store.get('rowsPerPage')) / newRowsPerPage
  );

  fetchNewData({ store, page: expectedPage, rowsPerPage: newRowsPerPage });
};

stories(
  module,
  __dirname,
  {
    Default({ store, count, page, rows, rowsPerPage, selected }) {
      const isRowSelected = id => selected.includes(id);

      const mappedRows = rows.map((row, index) => ({
        ...row,
        id: index,
        selected: isRowSelected(index),
      }));
      const pointer = boolean('Pointer', true);
      const checkboxes = boolean('Checkboxes', true);
      const disabled = boolean('Disabled', false);

      return (
        <Table>
          <TableHead>
            <TableRow heading checkboxes={checkboxes}>
              {columns.map(({ label }, index) => (
                <TableCell key={index} heading={true}>
                  {label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {mappedRows.map((row, index) => (
              <TableRow
                disabled={disabled}
                key={index}
                pointer={pointer}
                checkboxes={checkboxes}
                selected={row.selected}
                onClick={() => updateSelected({ store, id: row.id, selected })}
                onCheckboxClick={event => {
                  event.stopPropagation();
                  updateSelected({
                    store,
                    id: row.id,
                    multiSelect: true,
                    selected,
                  });
                }}
              >
                <TableCell>{row.caseNumber}</TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>{row.description}</TableCell>
                <TableCell>{row.component}</TableCell>
                <TableCell>{row.user}</TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <Pagination
                changeRowsPerPage={(newPage, newRowsPerPage) =>
                  changeRowsPerPage({
                    store,
                    page: newPage,
                    rowsPerPage: newRowsPerPage,
                  })
                }
                count={count}
                getNewPage={pageNum =>
                  fetchNewData({ store, page: pageNum, rowsPerPage })
                }
                labelDisplayedRows={({ count: newCount, from, to }) =>
                  `${from}–${to} van ${newCount}`
                }
                labelRowsPerPage={'Per pagina:'}
                page={page}
                rowsPerPage={rowsPerPage}
                rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
              />
            </TableRow>
          </TableFooter>
        </Table>
      );
    },
  },
  stores
);
