import { React, stories, select } from '../../story';
import Switch from './Switch';

const stores = {
  Default: {
    checked: false,
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store, checked }) {
      const onChange = ({ target }) => {
        store.set({
          checked: target.checked,
        });
      };

      return (
        <Switch
          variant={select('variant', ['regular', 'iOS'])}
          checked={checked}
          onChange={onChange}
        />
      );
    },
  },
  stores
);
