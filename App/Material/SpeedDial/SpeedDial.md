# 🔌 `SpeedDial` component

> *Material Design* **SpeedDial**.

## See also

- [`SpeedDial` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/SpeedDial)
- [`SpeedDial` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-speed-dial)

## External resources

- [*Material-UI* `SpeedDial` API](https://material-ui.com/lab/api/speed-dial/)
