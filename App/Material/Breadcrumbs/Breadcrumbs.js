import React from 'react';
import MUIBreadcrumbs from '@material-ui/lab/Breadcrumbs';
import { withStyles } from '@material-ui/core/styles';
import Icon from '../Icon/Icon';
import { breadcrumbStylesheet } from './Breadcrumbs.style';
import { getItemsToRender } from './library/functions';
import { addScopeAttribute, addScopeProp } from '../../library/addScope';

const ZERO = 0;
const ONE = 1;

const Separator = ({ classes }) => (
  <Icon
    size="small"
    classes={{
      root: classes.separator,
    }}
  >
    navigate_next
  </Icon>
);

/**
 * Default item renderer
 *
 * @param {Object} props
 * @param {Object} props.item
 * @param {Object} props.classes
 * @param {Number} props.index
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const defaultItemRenderer = ({
  classes,
  onItemClick,
  item,
  index,
  scope,
}) => (
  <a
    key={index}
    href={item.path}
    onClick={onItemClick}
    className={classes.link}
    {...addScopeAttribute(scope, `item-${index === ZERO ? 'first' : index}`)}
  >
    {item.label}
  </a>
);

/**
 * Default last item renderer
 *
 * @param {Object} props
 * @param {Object} props.item
 * @param {Object} props.classes
 * @param {Number} props.index
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const defaultLastItemRenderer = ({ item, classes, index, scope }) => (
  <span
    key={index}
    className={classes.last}
    {...addScopeAttribute(scope, 'item-last')}
  >
    {item.label}
  </span>
);

/**
 * *Material Design* **Breadcrumbs**.
 * - facade for a subset of `@material-ui/lab/Breadcrumbs`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Breadcrumbs
 * @see /npm-mintlab-ui/documentation/consumer/manual/Breadcrumbs.html
 * @see https://material-ui.com/api/icon/
 *
 * @param {Object} props
 * @param {Number} props.maxItems
 * @param {Array<Object>} props.items
 * @param {Function} props.onItemClick
 * @param {Object} props.classes
 * @param {Function} [props.itemRenderer=defaultItemRenderer]
 * @param {Function} [props.lastItemRenderer=defaultLastItemRenderer]
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
const Breadcrumbs = ({
  maxItems,
  items,
  onItemClick,
  classes,
  itemRenderer = defaultItemRenderer,
  lastItemRenderer = defaultLastItemRenderer,
  scope,
  ...restProps
}) => {
  const itemsToRender = maxItems ? getItemsToRender(items, maxItems) : items;

  return (
    <MUIBreadcrumbs
      separator={<Separator classes={classes} />}
      className={classes.breadcrumps}
      {...addScopeAttribute(scope, 'breadcrumbs')}
      {...restProps}
    >
      {itemsToRender &&
        itemsToRender.map((item, index) => {
          const itemProps = {
            item,
            index,
            onItemClick,
            classes,
            ...addScopeProp(scope, 'breadcrumbs'),
          };

          return index < itemsToRender.length - ONE
            ? itemRenderer(itemProps)
            : lastItemRenderer(itemProps);
        })}
    </MUIBreadcrumbs>
  );
};

export default withStyles(breadcrumbStylesheet)(Breadcrumbs);
