/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the TextField component
 * @param {Object} theme
 * @return {JSS}
 */
export const textFieldStyleSheet = ({
  palette: { error },
  mintlab: { greyscale, radius },
}) => ({
  formControl: {
    width: '100%',
    backgroundColor: greyscale.light,
    borderRadius: radius.textField,
  },
  inputRoot: {
    '&&': {
      marginTop: 0,
      '&&:before': {
        display: 'none', //removes the black default underline
      },
    },
  },
  inputRootErrorFocus: {
    background: error.light,
    borderRadius: radius.textField,
    '&&:after': {
      display: 'none',
    },
  },
  input: {
    padding: '14px',
  },
});
