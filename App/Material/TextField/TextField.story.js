import { React, stories, text, boolean } from '../../story';
import FormTextField, { GenericTextField, NewGenericTextField } from '.';
import Icon from '../../Material/Icon';
import InputAdornment from '@material-ui/core/InputAdornment';
import { Button } from '../Button/Button';

const stores = {
  'Form TextField': {
    value: '',
  },
  'Generic TextField': {
    value: '',
  },
  'New Generic TextField': {
    value: '',
  },
};

/**
 * @param {SyntheticEvent} event
 * @param {Object} store
 * @return {undefined}
 */
const onChange = ({ target: { value } }, store) => {
  store.set({ value });
};

stories(
  module,
  __dirname,
  {
    'Form TextField': function FormTextFieldStory({ store, value }) {
      const multiline = boolean('Multiline', false);
      const placeholder = text('Placeholder', 'Placeholder');
      const adornment = boolean('Adornment', false);
      const Adornment = (
        <InputAdornment position="end">
          <Button label="test" presets={['primary', 'medium', 'icon']}>
            alarm
          </Button>
        </InputAdornment>
      );

      return (
        <div
          style={{
            width: '300px',
          }}
        >
          <FormTextField
            info={text('Info', 'Type ahead')}
            label={text('Label', 'Text field')}
            error={text('Error', '')}
            required={true}
            value={value}
            onChange={evt => onChange(evt, store)}
            placeholder={placeholder}
            scope="story"
            isMultiline={multiline}
            {...adornment && { InputProps: { endAdornment: Adornment } }}
          />
        </div>
      );
    },
    'Generic TextField': function GenericTextFieldStory({ store, value }) {
      const startAdornment = boolean('Start adornment', true) ? (
        <Icon size="small">search</Icon>
      ) : null;
      const closeAction = boolean('Close', true) ? () => {} : null;

      return (
        <div
          style={{
            width: '300px',
          }}
        >
          <GenericTextField
            placeholder={text('Placeholder', 'Placeholder')}
            value={value}
            onChange={evt => onChange(evt, store)}
            startAdornment={startAdornment}
            closeAction={closeAction}
            scope="story"
          />
        </div>
      );
    },
    /* eslint complexity: [2, 5] */
    'New Generic TextField': function NewGenericTextFieldStory({
      store,
      value,
    }) {
      const startAdornment = boolean('Start adornment', true) ? (
        <Icon size="small">ballot</Icon>
      ) : null;
      const endAdornment = boolean('End adornment', true) ? (
        <Icon size="small">access_time</Icon>
      ) : null;
      const closeAction = boolean('Close', false) ? () => {} : null;

      return (
        <div
          style={{
            width: '300px',
          }}
        >
          <NewGenericTextField
            placeholder={text('Placeholder', 'Placeholder')}
            value={value}
            onChange={evt => onChange(evt, store)}
            startAdornment={startAdornment}
            endAdornment={endAdornment}
            closeAction={closeAction}
            scope="story"
          />
        </div>
      );
    },
  },
  stores
);
