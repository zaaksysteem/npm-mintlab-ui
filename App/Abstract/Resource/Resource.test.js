import React from 'react';
import { mount, shallow } from 'enzyme';
import { render } from '../../test';
import Resource, { DEFAULT_ID_ERROR } from '.';

const { assign } = Object;
const { isArray } = Array;

const TIMEOUT = 100;

const resources = {
  foo: 'FOO',
  bar: 'BAR',
};

describe('The `Resource` component', () => {
  describe('has an `isValidIdString` method', () => {
    const instance = new Resource();

    test('accepts a value that does not contain a comma', () => {
      const actual = instance.isValidIdString('$%^!@#');
      const asserted = true;

      expect(actual).toBe(asserted);
    });

    test('does not accept a value that contains a comma', () => {
      const actual = instance.isValidIdString('$%^!,@#');
      const asserted = false;

      expect(actual).toBe(asserted);
    });

    test('does not accept the empty string', () => {
      const actual = instance.isValidIdString('');
      const asserted = false;

      expect(actual).toBe(asserted);
    });

    test('does not accept a value that is not a string', () => {
      const actual = instance.isValidIdString();
      const asserted = false;

      expect(actual).toBe(asserted);
    });
  });

  describe('has an `isIdValueValid` method', () => {
    test('that accepts a valid string', () => {
      const instance = new Resource({
        id: '$%^&',
      });

      const actual = instance.isIdValueValid();
      const asserted = true;

      expect(actual).toBe(asserted);
    });

    test('that rejects an invalid string', () => {
      const instance = new Resource({
        id: '$%,^&',
      });

      const actual = instance.isIdValueValid();
      const asserted = false;

      expect(actual).toBe(asserted);
    });

    test('that accepts an array of valid strings', () => {
      const instance = new Resource({
        id: ['$%^&', '!@#$'],
      });

      const actual = instance.isIdValueValid();
      const asserted = true;

      expect(actual).toBe(asserted);
    });

    test('that rejects an array that contains an invalid strings', () => {
      const instance = new Resource({
        id: ['$%^&', '!@,#$'],
      });

      const actual = instance.isIdValueValid();
      const asserted = false;

      expect(actual).toBe(asserted);
    });
  });

  test('throws if the `id` prop is not valid', () => {
    const actual = () =>
      render(Resource, {
        id: '$%^,^&',
        resolve: jest.fn(),
      });

    expect(actual).toThrow(DEFAULT_ID_ERROR);
  });

  test('can throw with a custom error message if the `id` prop is not valid', () => {
    const idError = 'Bad dog!';
    const actual = () =>
      render(Resource, {
        id: '$%^,^&',
        idError,
        resolve: jest.fn(),
      });

    expect(actual).toThrow(idError);
  });

  test('renders a loader until the resource is resolved', () => {
    const wrapper = mount(
      <Resource
        id="foo"
        payload={{
          foo: null,
        }}
        resolve={() => {}}
      >
        <div />
      </Resource>
    );
    const actual = wrapper.find('Loader').length;
    const expected = 1;

    expect(actual).toBe(expected);
  });

  test('calls the `resolve` prop after mounting and receiving props', () => {
    const spy = jest.fn();
    const wrapper = shallow(
      <Resource
        id="foo"
        payload={{
          foo: null,
        }}
        resolve={spy}
      >
        <div />
      </Resource>
    );
    const expected = 2;

    wrapper.setProps({
      id: 'something',
    });

    expect(spy).toHaveBeenCalledTimes(expected);
  });

  // ZS-TODO: find a better way to handle these async tests
  {
    let wrapper;

    test('renders its child component with a resolved resource prop', done => {
      function resolveResource(id) {
        setTimeout(() => {
          wrapper.setProps({
            payload: {
              [id]: resources[id],
            },
          });

          const actual = wrapper.props().resource;
          const expected = {
            foo: 'FOO',
          };

          expect(actual).toEqual(expected);
          done();
        }, TIMEOUT);
      }

      wrapper = shallow(
        <Resource id="foo" resolve={resolveResource}>
          <div />
        </Resource>
      );
    });

    test('can set multiple resources', done => {
      function resolveResource(id) {
        setTimeout(() => {
          const payload = (isArray(id) ? id : [id]).reduce(
            (accumulator, key) =>
              assign(accumulator, {
                [key]: resources[key],
              }),
            {}
          );

          wrapper.setProps({
            payload,
          });

          const actual = wrapper.props().resource;
          const expected = {
            foo: 'FOO',
            bar: 'BAR',
          };

          expect(actual).toEqual(expected);
          done();
        }, TIMEOUT);
      }

      wrapper = shallow(
        <Resource id={['foo', 'bar']} resolve={resolveResource}>
          <div />
        </Resource>
      );
    });
  }
});
