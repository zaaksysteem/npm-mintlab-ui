/**
 * Use a *JSX* component instead of (yet) an(other) expression
 * to conditionally render a component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Render
 * @see /npm-mintlab-ui/documentation/consumer/manual/Render.html
 *
 * @param {Object} props
 * @param {ReactElement} props.children
 * @param {boolean} props.condition
 * @return {ReactElement|null}
 */
export const Render = ({ children, condition }) => {
  if (condition) {
    return children;
  }

  return null;
};

export default Render;
