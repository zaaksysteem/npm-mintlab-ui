import { getStyleFromHeight } from './getStyleFromHeight';
import { getStyleFromWidth } from './getStyleFromWidth';
import { reduceMap } from '@mintlab/kitchen-sink';

/**
 * `Svg` component utility function.
 *
 * @param {Array} viewBox
 * @param {string} [width] CSS length or percentage
 * @param {string} [height] CSS length or percentage
 * @param {string} [className]
 * @return {Object|null}
 */
export function getStyle(viewBox, width, height, className) {
  const map = new Map([
    // Set `width` and `height` as is.
    [() => width && height, () => ({ width, height })],
    // Calculate the style object based on  `width`.
    [() => width, () => getStyleFromWidth(viewBox, width)],
    // Calculate the style object based on `height`.
    [() => height, () => getStyleFromHeight(viewBox, height)],
    // Delegate sizing responsibility to `className`.
    [() => className, () => null],
  ]);

  return reduceMap({
    map,
    fallback: function getStyleFallback() {
      // Use the viewBox width/height as CSS pixel lengths.
      const [viewBoxWidth, viewBoxHeight] = viewBox;
      return {
        width: `${viewBoxWidth}px`,
        height: `${viewBoxHeight}px`,
      };
    },
  });
}
