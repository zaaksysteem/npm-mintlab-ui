import { getActionProps } from './MenuGroup';

describe('The `MenuGroup` component', () => {
  describe('uses a `getActionProps` function', () => {
    const action = () => null;

    test('sets component to `a` and target to `undefined` if only `href` is provided', () => {
      const actual = getActionProps({
        href: 'foo',
      });
      const expected = {
        component: 'a',
        href: 'foo',
        target: undefined,
      };

      expect(actual).toEqual(expected);
    });

    test('sets component to `a` and spreads the target value', () => {
      const actual = getActionProps({
        href: 'foo',
        target: 'bar',
      });
      const expected = {
        component: 'a',
        href: 'foo',
        target: 'bar',
      };

      expect(actual).toEqual(expected);
    });

    test('discards `action` if `href` is provided', () => {
      const actual = getActionProps({
        action,
        href: 'foo',
      });
      const expected = {
        component: 'a',
        href: 'foo',
        target: undefined,
      };

      expect(actual).toEqual(expected);
    });

    test('sets `onClick` to `action` if `href` is not provided', () => {
      const actual = getActionProps({
        action,
      });
      const expected = {
        onClick: action,
      };

      expect(actual).toEqual(expected);
    });
  });
});
