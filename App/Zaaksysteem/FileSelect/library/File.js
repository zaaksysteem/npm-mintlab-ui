import React from 'react';
import Card from '@material-ui/core/Card';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withStyles, withTheme } from '@material-ui/core/styles';
import classnames from 'classnames';
import { fileStylesheet } from './File.style';
import Icon from '../../../Material/Icon/Icon';
import Button from '../../../Material/Button/Button';
import Loader from '../../Loader/Loader';
import { addScopeAttribute, addScopeProp } from '../../../library/addScope';

const getLoader = ({ status, uploadProgress }) => {
  switch (status) {
    case 'pending':
      return uploadProgress > 0 ? (
        <LinearProgress variant="determinate" value={uploadProgress * 100} />
      ) : (
        <LinearProgress />
      );

    default:
      return null;
  }
};

const getIcon = ({ status, theme, classes }) => {
  switch (status) {
    case 'pending':
      return (
        <Loader
          color={theme.mintlab.greyscale.darker}
          className={classes.loader}
        />
      );
    case 'failed':
      return <Icon color="error">error_outline</Icon>;
    default:
      return (
        <Icon color={theme.mintlab.greyscale.darker}>insert_drive_file</Icon>
      );
  }
};

const File = ({
  name,
  className,
  classes,
  onDeleteClick,
  status = '',
  uploadProgress = 0,
  theme,
  scope,
  ...rest
}) => (
  <Card
    className={classnames(classes.card, className)}
    {...addScopeAttribute(scope, 'file')}
    {...rest}
  >
    <div className={classes.uploadProgress}>
      {getLoader({ status, uploadProgress })}
    </div>
    <div className={classes.flexContainer}>
      <div className={classes.iconContainer}>
        {getIcon({ status, theme, classes })}
      </div>
      <p className={classes.name}>{name}</p>
      {onDeleteClick && (
        <Button
          action={onDeleteClick}
          presets={['default', 'small', 'icon']}
          {...addScopeProp(scope, 'file', 'delete')}
        >
          close
        </Button>
      )}
    </div>
  </Card>
);

export default withTheme()(withStyles(fileStylesheet)(File));
