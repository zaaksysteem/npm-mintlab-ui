import { Component } from 'react';
import { bind, callOrNothingAtAll } from '@mintlab/kitchen-sink';
import filterOption from './library/filterOption';

export const DELAY = 400;
export const MIN_CHARACTERS = 3;
const MSG_CHOOSE = 'Select…';
const MSG_LOADING = 'Loading…';
const MSG_TYPE = 'Begin typing to search…';

export class SelectBase extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:choose': MSG_CHOOSE,
        'form:loading': MSG_LOADING,
        'form:beginTyping': MSG_TYPE,
      },
      disabled: false,
      autoLoad: false,
      filterOption,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.timeoutId = null;
    this.state = {
      focus: false,
    };
    bind(
      this,
      'handleBlur',
      'handleChange',
      'handleFocus',
      'handleInputChange',
      'handleKeyDown'
    );
  }

  // Lifecycle methods

  /**
   *  @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    if (this.props.autoLoad) {
      this.getChoices('');
    }
  }

  // Custom methods

  /**
   * Call form change function, if provided.
   *
   * @param {Object} value
   */
  handleChange(value) {
    const { onChange, name } = this.props;

    callOrNothingAtAll(onChange, () => [
      {
        target: {
          name,
          value,
          type: 'select',
        },
      },
    ]);
  }

  handleBlur() {
    const { onBlur, name } = this.props;
    this.setState({ focus: false });
    callOrNothingAtAll(onBlur, [
      {
        target: {
          name,
        },
      },
    ]);
  }

  handleFocus() {
    this.setState({ focus: true });
  }

  /**
   *
   * @param {Event} keyDownEvent
   */
  handleKeyDown(keyDownEvent) {
    const {
      props: { onKeyDown, name },
    } = this;

    const { key, keyCode, charCode } = keyDownEvent;

    keyDownEvent.stopPropagation();

    callOrNothingAtAll(onKeyDown, () => [
      {
        name,
        key,
        keyCode,
        charCode,
        type: 'select',
      },
    ]);
  }

  /**
   * @param {string} input
   * @return {boolean}
   */
  isValidInput(input) {
    return Boolean(input) && input.length >= MIN_CHARACTERS;
  }

  /**
   * @param {string} input
   */
  handleInputChange(input) {
    if (!this.isValidInput(input)) {
      return;
    }

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    this.timeoutId = setTimeout(() => {
      this.getChoices(input);
    }, DELAY);
  }

  /**
   * @param {*} input
   */
  getChoices(input) {
    const { getChoices } = this.props;

    if (!getChoices) {
      return;
    }

    getChoices(input);
  }
}

export default SelectBase;
