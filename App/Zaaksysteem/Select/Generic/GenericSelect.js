import React, { Fragment, createElement } from 'react';
import { withTheme } from '@material-ui/core/styles';
import ReactSelect from 'react-select';
import { cloneWithout } from '@mintlab/kitchen-sink';
import selectStyleSheet from './GenericSelect.style';
import SelectBase from '../Select.base';
import DropdownIndicator from '../library/DropdownIndicator';
import { bind } from '@mintlab/kitchen-sink';
import { components } from 'react-select';
import Render from '../../../Abstract/Render';
import ClearIndicator from '../library/ClearIndicator';

const { assign } = Object;

/**
 * Choices and value must be provided in the form of a single, or array of objects.
 *
 * @typedef {Object} SelectValue
 * @property {string} value
 * @property {string} label
 * @property {Array<string>} alternativeLabels
 * @example
 * {
 *   value: 'strawberry',
 *   label: 'Strawberry',
 *   alternativeLabels: ['Fruit', 'Red'],
 * }
 */

/**
 * Facade for React Select v2.
 * - additional props are passed through to that component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Select
 * @see /npm-mintlab-ui/documentation/consumer/manual/Select.html
 * @see https://react-select.com/home
 *
 * @reactProps {boolean} [autoLoad=false]
 * @reactProps {SelectValue|Array<SelectValue>} choices
 * @reactProps {Function} filterOption
 * @reactProps {boolean} [disabled=false]
 * @reactProps {Function} getChoices
 * @reactProps {string} name
 * @reactProps {Function} onChange
 * @reactProps {Object} styles
 * @reactProps {Object} translations
 * @reactProps {*} startAdornment
 * @reactProps {SelectValue|Array<SelectValue>} value
 * @reactProps {boolean} loading
 */
export class GenericSelect extends SelectBase {
  constructor(props) {
    super(props);
    bind(this, 'Control', 'DropdownWrapper');
  }

  /* eslint complexity: [2, 4] */
  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        choices: options,
        name,
        translations,
        theme,
        disabled,
        autoLoad,
        styles,
        hasInitialChoices,
        value,
        filterOption,
        loading,
        ...rest
      },
      state: { focus },
      handleInputChange,
      handleFocus,
      handleChange,
      handleBlur,
      handleKeyDown,
      Control,
      DropdownWrapper,
    } = this;

    const shouldHandleInputChange = () => !autoLoad && !hasInitialChoices;

    return (
      <Fragment>
        <ReactSelect
          isDisabled={disabled}
          cacheOptions={true}
          isLoading={loading}
          value={value}
          options={options}
          loadingMessage={() => translations['form:loading']}
          noOptionsMessage={() => null}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onInputChange={shouldHandleInputChange() && handleInputChange}
          onKeyDown={handleKeyDown}
          placeholder={translations['form:choose']}
          classNamePrefix="react-select"
          name={name}
          components={{
            ClearIndicator,
            Control,
            DropdownIndicator: DropdownWrapper,
          }}
          styles={
            styles ||
            selectStyleSheet({
              theme,
              focus,
            })
          }
          filterOption={filterOption}
          {...cloneWithout(
            rest,
            'name',
            'value',
            'onBlur',
            'onChange',
            'onFocus',
            'onKeyDown'
          )}
        />
      </Fragment>
    );
  }

  /**
   * @param {Object} selectProps
   * @return {ReactElement}
   */
  Control(selectProps) {
    const { startAdornment } = this.props;

    return (
      <components.Control {...selectProps}>
        <Render condition={startAdornment}>
          <div className="startAdornment">{startAdornment}</div>
        </Render>
        {selectProps.children}
      </components.Control>
    );
  }

  /**
   * @param {Object} selectProps
   * @return {ReactElement}
   */
  DropdownWrapper(selectProps) {
    const { getChoices, choices } = this.props;

    return createElement(
      DropdownIndicator,
      assign({}, selectProps, { getChoices, choices })
    );
  }
}

export default withTheme()(GenericSelect);
