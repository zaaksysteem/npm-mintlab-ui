ARG WORKSPACE=/home/node/ui

#### Stage: update npm and create working directory
FROM node:carbon-alpine AS base
ARG WORKSPACE
RUN npm install -g npm@6.4.1\
  && mkdir $WORKSPACE\
  && chown -R node:node $WORKSPACE

#### Stage: install dependencies
FROM base AS install
USER node
ARG WORKSPACE
WORKDIR $WORKSPACE
# Copy package.json and lock files to root
COPY --chown=node:node ./root/*.json ./
RUN npm install --no-optional --loglevel error

#### Stage: copy files
FROM install AS files

# copy dot files to root
COPY --chown=node:node ./root/.* ./

# The README is used to generate the documentation cover page
COPY --chown=node:node ./README.md ./README.md
COPY --chown=node:node ./App ./App
COPY --chown=node:node .prettierrc .prettierrc
COPY --chown=node:node ./node ./node
COPY --chown=node:node ./root ./root

#### Stage: build distribution
FROM files AS distribution

RUN npm run lint\
  && npm run test\
  && npm run webpack

#### Stage: set up development environment
FROM distribution AS development
RUN rm ./package*.json\
  && ln -s ./root/package.json ./package.json\
  && ln -s ./root/package-lock.json ./package-lock.json
